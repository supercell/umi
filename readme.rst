umi
===

A static site generator.

The purpose of creating ``umi`` was to fufil what is probably every developer's
desire at some point: to create their own static site generator.  My goals were
to create one that:

* Doesn't require third-party dependencies
* Has all code within one file.

I chose Python to complete this task, since it has an extensive standard
library, plus it's fairly simple for other people to learn if they're not
familiar with the language.

Prerequisites
-------------

Python >= 3.4
 https://python.org


Usage
-----

Within an empty directory, run ``umi -n``. A new file named ``umi.conf`` will be
created, along with three directories: content, output, and templates.


You can view the included example site (in the `example directory`_) to view
more information about ``umi``, as well as details about the newly created
directories/files.

For command-line help, just run ``umi --help``.

.. _`example directory`: https://codeberg.org/supercell/umi/src/branch/master/example

Roadmap
-------

There are still some features that I'd like to implement in ``umi``. None of these are guaranteed to be implemented though :o

Neocities integration
 I use Neocities for hosting `my website`_. It'd be nice to support uploading any modified files via some command (for example: ``umi upload``).

.. _`my website`: https://yume-neru.neocities.org

License
-------

``umi`` is licensed under the Apache License, Version 2.0.  you read a copy in
the included ``license.txt`` file, or online at
http://www.apache.org/licenses/LICENSE-2.0.txt
