#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2020-2024 mio <stigma@disroot.org>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# umi.py

#####
#
#
# Imports
#
#
###########

# standard library imports
import argparse
import os
import re
import shutil
import sys
import time

import configparser
from html.parser import HTMLParser

RESET = '\033[0m'
BOLD = '\033[1m'

RED = '\033[;31m'
GREEN = '\033[;32m'
YELLOW = '\033[;33m'
MAGENTA = '\033[;35m'
CYAN = '\033[;36m'

DEFAULT_STATUS_TEMPLATE = """
<div class="status-feed">
    <status-start>
        <div class="status">
            <p>%{content}%</p>
            <p>&mdash;%{timestamp}%</p>
        </div>
    </status-start>
</div>
"""

# Utility functions


START_TIME = time.time_ns() // 1_000_000
last_checkpoint = START_TIME


def color(msg, *colors):
    return ''.join(colors) + msg + RESET


def checkpoint(msg):
    global last_checkpoint
    now = time.time_ns() // 1_000_000
    print('{}[{}ms +{}ms]{} {}'.format(
        YELLOW, now - START_TIME, now - last_checkpoint, RESET, msg
    ))
    last_checkpoint = now


def source_is_newer(source, dest):
    try:
        return os.stat(source).st_mtime > os.stat(dest).st_mtime
    except FileNotFoundError:
        return True

#####
#
#
# Functions relating to command line options
#
#
###########


def new(conf_path):
    """create a new project

    if new_project is true, create the folder structure as well
    """
    conf = configparser.ConfigParser()
    # since the user can call --new even when a config exists, check for it
    # first
    if not os.path.exists(conf_path):
        conf.add_section("general")
        conf.set("general", "content_directory", "content")
        conf.set("general", "output_directory", "output")
        conf.set("general", "template_directory", "templates")

        conf.add_section("template.header")
        conf.set("template.header", "filename", "header.html")
        conf.set("template.header", "selector", "header#top")

        with open(conf_path, "w") as conf_file:
            conf.write(conf_file)

    conf.read(conf_path)

    # create the default folder structure
    folders = [
        conf.get('general', 'content_directory', fallback='content'),
        conf.get('general', 'output_directory', fallback='output'),
        conf.get('general', 'template_directory', fallback='templates')
    ]

    for folder in folders:
        if not os.path.exists(folder):
            os.mkdir(folder)

    # create the default index file
    content_index_path = os.path.join(folders[0], "index.html")
    if not os.path.exists(content_index_path):
        with open(content_index_path, "w") as f:
            f.write("""<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>site title</title>
        <link rel="stylesheet" type="text/css" href="style.css"/>
    </head>
    <body>
        <header id="top"></header>
        <main>
          <h1>hello from umi!</h1>
            <p>this is just a quick-start, you should check
            <a href="https://codeberg.org/supercell/umi/src/branch/master/example">
                the example site</a>
            for a better demo.
          </p>
        </main>
    </body>
</html>\n""")

    # create the default css file
    output_css_path = os.path.join(folders[1], "style.css")
    if not os.path.exists(output_css_path):
        with open(output_css_path, "w") as f:
            f.write("""body {
    background-color: #5077a1;
    color: white;
    font-family: sans, sans-serif;
    text-align: center;
}

a { color: greenyellow; }\n""")

    # create the default header template
    template_ex_path = os.path.join(folders[2], "header.html")
    if not os.path.exists(template_ex_path):
        with open(template_ex_path, "w") as f:
            f.write("""<header>
    <nav>
        <a href="/">home</a>
    </nav>
</header>\n""")


def version():
    """Print version information"""
    print("umi 2.0-dev (2024-02-11)")
    print("Copyright (C) 2020-2024 mio.")
    print("License: Apache-2.0 <http://www.apache.org/licenses/LICENSE-2.0>")
    print("There is NO WARRANTY, to the extent permitted by law.")


#####
#
#
# Utility Functions (part 2 (woops!))
#
#
###########

def copy_directory(src, dst):
    """Copy a directory and all of it's files into another directory.

    Parameters:
        src (str): The source directory that will be copied.
        dst (str): The destination directory, where src will be copied to.

    Returns:
        bool: Returns True if all items from src were copied to dst.
              Otherwise, return false.
    """
    if not os.path.exists(src):
        return False

    if not os.path.exists(dst):
        os.makedirs(dst)
        shutil.copystat(src, dst)

    tree = os.listdir(src)
    for leaf in tree:
        src_file = os.path.join(src, leaf)
        dst_file = os.path.join(dst, leaf)
        if os.path.isdir(src_file):
            copy_directory(src_file, dst_file)
        else:
            shutil.copy2(src_file, dst_file)

    return True

#####
#
#
# Templating Functions
#
#
###########


def read_template(template, temp_dir):
    template_path = os.path.join(temp_dir, template["filename"])
    try:
        return open(template_path, "r").read()
    except FileNotFoundError:
        print("ERROR: Template not found: {}".format(template_path))
        print("this might be simply because the file doesn't exist,",
              "or because it's incorrect in your umi.conf")
        sys.exit(1)


#####
#
#
# Status Feed Functions
#
#
###########

def parse_status_setting(line):
    from re import split
    splits = split(r"\s+", line)
    key = splits[1]
    value = ' '.join(splits[3:])
    return (key, value)


def parse_status(line):
    from re import split
    matches = split(r"\t", line, maxsplit=1)

    if len(matches) != 2:
        print(f"{color('error:', RED, BOLD)} failed to parse status:\n\t" +
              f"{line.strip()}\n\nIs there a tab character separating the " +
              "date and status content?", file=sys.stderr)
        return None

    return matches[0], matches[1]


def insert_status_feed(template, statuses, nick, avatar, config, dirs):
    from datetime import datetime
    from re import search

    start_line = None
    end_line = None
    status_content_list = list()
    content_pre = ''
    content_post = ''
    output_html = list()

    for line_number, line in enumerate(template.split('\n')):
        avatar_search = search(r"%\{avatar\}%", line)
        if avatar_search is not None:
            if avatar is None:
                print(color('warning:', MAGENTA, BOLD),
                      'tried to replace avatar, but no avatar set in config',
                      file=sys.stderr)
            else:
                line = line.replace("%{avatar}%", avatar)

        nick_search = search(r"%\{nickname}%", line)
        if nick_search is not None:
            line = line.replace("%{nickname}%", nick)

        # If <status-start> is found, append everything before the
        # tag to content_pre (part of overall feed HTML), and prepend
        # everything after the tag to status_content_list (part of the
        # individual status)
        start_search = search(r"<status-start>", line)
        if start_search is not None:
            start_line = line_number
            status_content_list.append(line[start_search.end():])
            content_pre += line[0:start_search.start()]
            output_html.append(content_pre)
            continue

        # If we haven't found the <status-start>, then this is a part
        # of content_pre. We then continue on to the next line.
        if start_line is None:
            content_pre += line
            continue

        # Similar to the above for <status-start>, except for the
        # closing tag.
        end_search = search(r"</status-start>", line)
        if end_search is not None:
            end_line = line_number
            status_content_list.append(line[0:end_search.start()])
            content_post += line[end_search.end():]
            continue

        if end_line is None:
            status_content_list.append(line)
        else:
            content_post += line

    timestamp_format = config.get('statuses', 'timestamp_format',
                                  raw=True, fallback='%x')

    for status in statuses:
        if status is None:
            print(color('warning:', MAGENTA, BOLD),
                  'skipping a status since it is invalid.')
            continue
        datetime_ = datetime.strptime(status[0], "%Y-%m-%dT%H:%M:%S")
        output_html.append(
            '\n'.join(status_content_list)
            .replace('%{content}%', status[1])
            .replace('%{timestamp}%', datetime_.strftime(timestamp_format))
        )

    output_html.append(content_post)
    return "\n".join(output_html)


#####
#
#
# HTML Parsing
#
#
###########

def umi_parse_html(templates, html_file, notes_file, config, dirs):
    p = UmiHTMLParser(templates)
    out_text = []
    for line in html_file.readlines():
        p.feed(line)
        # FIXME: Better name than 'to_edit'
        #        actually, probably just need to redo this section.
        if p.to_edit:
            line = read_template(p.template, dirs["templates"])
            p.to_edit = False

        if p.wants_notes:
            if not notes_file or not os.path.exists(notes_file):
                print("WARN: Wanted status feed, but couldn't find file or" +
                      " no 'notes' file was provided in umi.conf.",
                      file=sys.stderr)
                p.wants_notes = False
            else:
                # These are the configuration options for umi's status feed
                nickname = None
                avatar = None
                # Each status line can be a tuple (timestamp, content)
                statuses = []
                # The remaining number of notes
                remaining_notes = p.notes_amount
                with open(notes_file, 'r') as nf:
                    for line in nf:
                        if remaining_notes <= 0:
                            break
                        # TODO: For v2, change these around.
                        # e.g.: #nick=nickname instead of # nick=nickname
                        # having # comment makes more sense the #comment
                        if line.startswith("# "):
                            setting = parse_status_setting(line)
                            if setting[0] == "nick":
                                nickname = setting[1]
                            elif setting[0] == "avatar":
                                avatar = setting[1]
                            continue
                        elif line.startswith('#'):
                            # Normal comment.
                            continue

                        if "" == line.strip():
                            continue

                        # Parse the first $p.notes_amount lines
                        parsed_status = parse_status(line)
                        statuses.append(parsed_status)
                        remaining_notes -= 1

                status_template_file = config.get('statuses', 'template',
                                                  fallback=None)

                if status_template_file is None:
                    line = insert_status_feed(DEFAULT_STATUS_TEMPLATE,
                                              statuses, nickname, avatar,
                                              config, dirs)
                else:
                    status_template_file = os.path.join(dirs['templates'],
                                                        status_template_file)
                    status_template = open(status_template_file).read()
                    line = insert_status_feed(status_template, statuses,
                                              nickname, avatar, config, dirs)

                p.wants_notes = False

        out_text.append(line)

    return ''.join(out_text)


class UmiHTMLParser(HTMLParser):
    def __init__(self, templates):
        HTMLParser.__init__(self)
        self.templates = templates
        self.to_edit = False
        self.wants_notes = False
        self.notes_amount = 2
        self.template = {}

    def handle_starttag(self, tag, attributes):
        # Notes
        if tag == "umi-statuses":
            str_amount = '2'
            for name, value in attributes:
                if name == "amount":
                    str_amount = value
                    break

            try:
                amount = int(str_amount)
            except ValueError:
                print(
                    color('[warning]:', MAGENTA, BOLD) +
                    ' failed to convert umi-notes amount to number: {}'.format(
                        str_amount
                    ))
                print('           Falling back to 2 statuses.')
                amount = 2

            self.wants_notes = True
            self.notes_amount = amount
            return

        # Templates
        for template in self.templates.items():
            dictionary = template[1]
            if not tag == dictionary["element"]:
                continue

            for name, value in attributes:
                if name == "id" and dictionary["id"] in value:
                    self.template = dictionary
                    self.to_edit = True
                    break
                elif name == "id":
                    # umi doesn't use any other attributes, so may as well
                    # move on to the next template.
                    break
                else:
                    # continue until we get an 'id' or the end.
                    continue

            if self.to_edit:
                return


class UmiHTMLTemplateLocator(HTMLParser):
    """
    Locates any templates that are used in the file that is being fed to
    the parser.

    Really, this could be replaced by UmiHTMLParser.
    """
    def __init__(self, templates):
        HTMLParser.__init__(self)
        self.templates = templates
        self.encountered_template = False
        self.template = None
        self.encountered_statuses = False

    def handle_starttag(self, tag, attributes):
        # Check for <umi-statuses>. Templates could still appear, so
        # don't return.
        if tag == 'umi-statuses':
            self.encountered_statuses = True

        # Templates are specified by the 'id' attribute, so check
        # that the element *has* an id attribute.
        try:
            id_attr = next(attr for attr in attributes if attr[0] == 'id')
        except StopIteration:
            return

        for template in self.templates.items():
            dictionary = template[1]
            if tag != dictionary['element']:
                continue

            if id_attr[1] == dictionary['id']:
                self.encountered_template = True
                self.template = dictionary
                break
#####
#
#
# Main program
#
#
###########


def parse_arguments():
    """
    Parse command line arguments using Python ArgumentParser
    and return the parsed arguments object.
    """
    parser = argparse.ArgumentParser(description="A static site generator")
    parser.add_argument("-c", "--config", default="umi.conf",
                        help="Configuration file to parse (default: umi.conf)")
    parser.add_argument("-n", "--new", action="store_true",
                        help="Create a new site in the current directory")
    parser.add_argument("-v", "--version", action="store_true",
                        help="Display version information and exit")

    return parser.parse_args()


def process_arguments(args):
    """
    Process the parsed arguments.

    Returns:
        None: If `args.version` or `args.new` are True
        configparser.ConfigParser: A parsed `umi.conf` file.
    """
    if args.version:
        version()
        return None

    config_path = os.path.join(os.getcwd(), args.config)

    if args.new:
        new(config_path)
        return None

    config = configparser.ConfigParser()
    config.read(os.path.join(os.getcwd(), args.config))

    return config


def setup_dirs(config):
    """
    Set up a dictionary containing all the directories that umi uses
    based off of the configuration file.

    Parameters:
        config (configparser.ConfigParser): A ConfigParser object that
                                        has been set up.

    Returns:
        dict: A dictionary containing keys for the 'output', 'templates',
              and 'content' directories.
    """
    dirs = dict()
    dirs['output'] = config.get('general', 'output_directory',
                                fallback='output')
    dirs['templates'] = config.get('general', 'template_directory',
                                   fallback='templates')
    dirs['content'] = config.get('general', 'content_directory',
                                 fallback='content')

    return dirs


def parse_templates(config):
    """
    Process all the [template.*] sections in the configuration file
    and return a dictionary containing each template with the respective
    filename, element, and id.

    Parameters:
        config (configparser.ConfigParser): A ConfigParser object that has
                                        been set up.

    Returns:
        dict: A dictionary containing the keys relating to the template
              sections in the configuration file.
    """
    sections = dict()
    for section in config.sections():
        # Check if the current section is for a template
        # [template.<TEMPLATENAME>]
        title = re.search(r"template.\w+", section)
        if title:
            try:
                selector = config.get(title.group(), "selector")
            except configparser.NoOptionError:
                print(color('error:', RED, BOLD), 'umi could not find',
                      'the selector for template "{}"'.format(title.group()))
                return None

            html_tag, html_id = selector.rsplit('#')

            try:
                filename = config.get(title.group(), "filename")

                template_path = os.path.join(
                    config.get('general', 'template_directory',
                               fallback='templates'),
                    filename)
                modification_date = os.stat(template_path).st_mtime
            except configparser.NoOptionError as e:
                print(color('error:', RED, BOLD),
                      'umi could not find the file for template "',
                      title.group(), '"')
                print('       {}'.format(e))
                return None

            sections[title.group()] = {
                "filename": filename,
                "path-from-root": template_path,
                "element": html_tag,
                "id": html_id,
                "modification-date": modification_date
            }

    return sections


def check_html_needs_update(filename, output_filename, templates, notes_file):
    """
    Determine if an HTML file needs a re-write.
    """
    # TODO: the parser should probably be merged with UmiHTMLParser.
    parser = UmiHTMLTemplateLocator(templates)
    with open(filename) as file:
        parser.feed(file.read())
        if parser.encountered_template:

            # Template has been updated since last build
            if source_is_newer(parser.template['path-from-root'],
                               output_filename):
                return True

            # content/filename has been updated since the last build.
            if source_is_newer(filename, output_filename):
                return True

            parser.encountered_template = False
        if parser.encountered_statuses:
            if notes_file is None:
                print(color('error:', RED, BOLD),
                      'Encountered an <umi-statuses> element, but no "notes" file!',
                      file=sys.stderr)
                return False
            return source_is_newer(notes_file, output_filename)

    return False


def check_needs_update(dirs, templates, filename, notes_file):
    """
    Check if *filename* has been updated since last build, or if any
    templates that *filename* use have been updated since last build.
    """
    _, ext = os.path.splitext(filename)

    # TODO: Possible method to add hook for extensions?
    #       Maybe not in this function itself, but perhaps
    #       it could be repurposed to process 'metadata' about
    #       the file (for example: HTML templates, Markdown frontmatter)
    #       and return related information.
    if ext == '.html':
        output_filename = filename.replace(dirs['content'], dirs['output'])
        return check_html_needs_update(filename, output_filename, templates, notes_file)

    return False


def crawl_files(config, content_dir, excluded_filetypes):
    """
    Get all files in a directory, recursively.
    """
    files = list()
    for entry in os.scandir(content_dir):
        _, ext = os.path.splitext(entry.name)
        if ext in excluded_filetypes:
            continue
        if entry.is_dir():
            files = files + crawl_files(config,
                                        os.path.join(content_dir, entry.name),
                                        excluded_filetypes)
        else:
            files.append(os.path.join(content_dir, entry.name))
    return files


def process_files(config, templates, dirs):
    """
    Process all the files in the content directory.

    Parameters:
        config (configparser.ConfigParser): The parsed umi.conf file.
        templates                   (dict): The dictionary containing all template files
        dirs                        (dict): The dictionary containing all the directories

    Returns:
        This function does not return anything.
    """
    # Retrieve the ignored filetypes from the configuration
    try:
        ignored_filetypes = config.get("general", "ignore").split(',')
    except configparser.NoOptionError:
        ignored_filetypes = []

    notes_file = config.get('general', 'notes_file', fallback=None)

    checkpoint('indexing')

    all_files = crawl_files(config, dirs['content'], ignored_filetypes)
    document = {
        filename: {
            'output': filename.replace(dirs['content'], dirs['output']),
            'needs-update': check_needs_update(dirs, templates, filename, notes_file)
        } for filename in all_files
    }

    checkpoint('building')
    to_build = filter(lambda kv: kv[1]['needs-update'], document.items())
    for file_info in to_build:
        _, ext = os.path.splitext(file_info[0])
        dirname = os.path.dirname(file_info[1]['output'])
        
        os.makedirs(dirname, exist_ok=True)

        # TODO: Another possible hook for extensions.
        #       See the TODO in check_needs_update
        if ext == '.html':
            print(color('[render]', CYAN, BOLD), file_info[0])
            with open(file_info[0]) as input_file:
                text = umi_parse_html(templates, input_file, notes_file,
                                      config, dirs)

            with open(file_info[1]['output'], 'w') as output_file:
                output_file.write(text + '\n')


def main(argv):
    """
    Main program function.

    Parameters:
        argv (list): A list of str arguments passed to the program

      Returns:
        int: An exit code for the program relating to if it ran
             successfully.
    """
    parsed_args = parse_arguments()
    config = process_arguments(parsed_args)

    # config will be None if --version or --new was passed
    # as a command line argument
    if config is None:
        return True

    checkpoint('processing templates')
    dirs = setup_dirs(config)
    template_sections = parse_templates(config)

    # template_sections will be None if it failed to parse
    # all the templates correctly. An error message is
    # printed.
    if template_sections is None:
        return False

    # No need to parse templates if there are none.
    if 0 >= len(template_sections):
        print("WARNING: No templates defined in umi's configuration file,",
              "so template parsing will be skipped.")
        copy_directory(dirs["content"], dirs["output"])
        return True

    process_files(config, template_sections, dirs)

    return True


if __name__ == "__main__":
    try:
        checkpoint("loaded")
        success = main(sys.argv)
        if success:
            checkpoint(color('done!', GREEN, BOLD))
    except KeyboardInterrupt:
        print("Interrupt: stopping umi.")
        sys.exit(1)
