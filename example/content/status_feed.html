<!DOCTYPE html>
<html lang="en">
    <head id="site-head"></head>
    <body>
      <header id="main-header"></header>
      <main>
        <h1>status feed</h1>
        <p>
            as of <time datetime="2021-07-03">3<sup>rd</sup> July, 2021</time>,
            umi supports a <q>status feed</q>. this
            <em>will<sup id="rt1"><a href="#fn1">1</a></sup></em> allow you to
            quickly post small updates to various places on your site.  there
            are three parts to making this work in umi, the first is the HTML:
        </p>
        <pre><code>&lt;umi-statuses amount="5" /&gt;</code></pre>
        <p>
            this will display a feed, similar to the one below:
        </p>
        <umi-statuses amount="5" />
        <p>
            to explain the HTML just a little: the <tt>amount</tt> attribute
            limits the amount of statuses to display. there is no way to display
            infinite statuses.  the reason only 3 statuses are being shown
            despite specifying 5, is because there are only 3 statuses in <q>the
            file</q> that is read.
        </p>
        <h2>the file</h2>
        <p>
            which leads me into the second part of making this work, <q>the file</q>.
            <q>the file</q> is formatted similar to
            <a href="https://twtxt.readthedocs.io/en/stable/user/twtxtfile.html#format-specification"
                title="twtxt file format specification">twtxt</a>. most lines in
            the <q>the file</q> will follow this format:
        </p>
            <pre>YYYY-MM-DDTHH:MM:SS0x09STATUS_CONTENT</pre>
        <p>
            looks messy, but, quite simply, up until the <tt>0x09</tt> is just a
            standard time format (specifically
            <a href="https://en.wikipedia.org/wiki/ISO_8601"
				title="Wikipedia - ISO8601">ISO8601</a> /
			<a href="https://datatracker.ietf.org/doc/html/rfc3339"
				title="IETF - RFC339">RFC3339</a>).
			The <tt>0x09</tt> is a single horizontal tab character, I just used
			the hex code because I can't really <em>show</em> the tab. finally,
			as you probably have guessed, <tt>STATUS_CONTENT</tt> is the content
			of the status. everything after the first tab until the end of the
			line (<tt>\n</tt>) is part of the status content.
        </p>
        <p>
            the only other type of line supported is the configuration/comment
            line. by placing a # at the start of the line, umi will treat it
            as a comment. <em>if</em> the # is followed by a space, umi will
            attempt to parse the line as a configuration setting for statuses.
        </p>
        <p>
            at the moment &mdash; and foreseeable future &mdash; umi only
            supports two configuration settings in "the file". these are:
            <tt>avatar</tt> and <tt>nick</tt>. these can be anything you
			like, but the idea is that <tt>avatar</tt> is an image URL,
			while <tt>nick</tt> is your nickname. these can be used for
			custom templates, which is the final (optional) piece of the
			status feed.
        </p>
        <p>
			the final step is to specify where "the file" is located. in your
			<tt>umi.conf</tt> file, in the <tt>[general]</tt> section, add a
			new line:
        </p>
        <pre><code>notes_file = FILE_LOCATION</code></pre>
        <p>
			make sure to replace the <tt>FILE_LOCATION</tt> with the path of
			"the file".
        </p>
        <h2>custom templates</h2>
        <p>
            while this section is optional, there is a little bit too it. the
            first is two settings in <tt>umi.conf</tt>, neither are required
            (i.e. you can use either one without the other, or none!).
        </p>
        <p>
            the settings are: <tt>template</tt> and <tt>timestamp_format</tt>,
            the later is how you want your (optional) timestamp to be formatted
            in the custom template, e.g., <code>%A %d %B, %Y</code> ouputs a string
            similar to <tt>Saturday 3 July, 2021</tt>. a list of timestamp formatting options can be found at the <a href="#timestamp-formatting">bottom of this page</a>. the <tt>template</tt>
            setting is the file (within your <tt>template_dir</tt>) to use
            for creating the statuses.
        </p>
        <p>
            the way in which you can construct the custom template is as follows:
            umi looks for a custom HTML element's start and end tag
            (<tt>&lt;status-start&gt;</tt> and <tt>&lt;/status-start&gt;</tt>).
            <em>anything</em> between these two tags will be repeated for every
            status in <q>the file</q>. you can put HTML outside of these tags and
            they will be kept, but not repeated.
        </p>
        <p>
            within the <tt>status-start</tt> tags you can use a few
            <q>variables</q> which are replaced by umi. these <q>variables</q>
            take the form of <tt>%{VARIABLE_NAME}%</tt>. the potential variables
            are:
        </p>
        <ul>
            <li><tt>avatar</tt> &mdash; the avatar set in <q>the file</q>.</li>
            <li><tt>nickname</tt> &mdash; the <tt>nick</tt> set in <q>the file</q>.</li>
            <li><tt>content</tt> &mdash; the content of the status.</li>
            <li><tt>timestamp</tt> &mdash; the timestamp of the status.</li>
        </ul>
        <p>
            as an example, here is the one used on this page:
        </p>
        <pre style="overflow-x:scroll;"><code>&lt;div class="status-feed"&gt;
    &lt;status-start&gt;
        &lt;div class="status"&gt;
            &lt;p&gt;&lt;span class="status-byline"&gt;On %{timestamp}%, %{nickname}%&lt;img src="%{avatar}%" class="avatar" /&gt; posted:&lt;/span&gt;&lt;br /&gt;
            &lt;br /&gt;
            %{content}%&lt;/span&gt;&lt;/p&gt;
        &lt;/div&gt;
    &lt;/status-start&gt;
&lt;/div&gt;
</code></pre>
        <p>
            the <tt>status-start</tt> tags are removed from the final output.
        </p>
        <h3 id="timestamp-formatting">timestamp formatting</h3>
        <p>
			the following table was copied and modified from the Python docs.
			the modifications are just the removal of some notes due to their
			irrelevance for being on <em>this</em> page.
        </p>
        <table>
			<caption>&copy; Copyright 1990-2020, Python Software Foundation</caption>
			<colgroup>
				<col style="width:10%;" />
				<col style="width:45%;" />
				<col style="width:30%;" />
				<col style="width:9%;" />
			</colgroup>
			<tr>
				<th>Directive</th>
				<th>Meaning</th>
				<th>Example</th>
				<th>Notes</th>
			</tr>
			<tr>
				<td><tt>%a</tt></td>
				<td>Weekday as locale's abbreviated name.</td>
				<td>
					Sun, Mon, &hellip;, Sat (en_US); <br />
					So, Mo, &hellip;, Sa (de_DE)
				</td>
				<td>(1)</td>
			</tr>
			<tr>
				<td><tt>%A</tt></td>
				<td>Weekday as locale's full name.</td>
				<td>
					Sunday, Monday, &hellip;, Saturday (en_US);<br />
					Sonntag, Montag, &hellip;, Samstag (de_DE)
				</td>
				<td>(1)</td>
			</tr>
			<tr>
				<td><tt>%w</tt></td>
				<td>
					Weekday as a decimal number, where 0 is Sunday and 6 is
					Saturday.
				</td>
				<td colspan="2">0, 1, &hellip;, 6</td>
			</tr>
			<tr>
				<td><tt>%d</tt></td>
				<td>Day of the month as a zero-padded decimal number.</td>
				<td colspan="2">01, 02, &hellip;, 31</td>
			</tr>
			<tr>
				<td><tt>%b</tt></td>
				<td>Month as locale’s abbreviated name.</td>
				<td>
					Jan, Feb, &hellip;, Dec (en_US);<br />
					Jan, Feb, &hellip;, Dez (de_DE)
				</td>
				<td>(1)</td>
			</tr>
			<tr>
				<td><tt>%B</tt></td>
				<td>Month as locale’s full name.</td>
				<td>
					January, February, &hellip;, December (en_US);<br />
					Januar, Februar, &hellip;, Dezember (de_DE)
				</td>
				<td>(1)</td>
			</tr>
			<tr>
				<td><tt>%m</tt></td>
				<td>Month as a zero-padded decimal number.</td>
				<td colspan="2">01, 02, &hellip;, 12</td>
			</tr>
			<tr>
				<td><tt>%y</tt></td>
				<td>Year without century as a zero-padded decimal number.</td>
				<td colspan="2">00, 01, &hellip;, 99</td>
			</tr>
			<tr>
				<td><tt>%Y</tt></td>
				<td>Year with century as a decimal number.</td>
				<td colspan="2">1970, 1988, 2001, 2013</td>
			</tr>
			<tr>
				<td><tt>%H</tt></td>
				<td>Hour (24-hour clock) as a zero-padded decimal number.</td>
				<td colspan="2">00, 01, &hellip;, 23</td>
			</tr>
			<tr>
				<td><tt>%I</tt></td>
				<td>Hour (12-hour clock) as a zero-padded decimal number.</td>
				<td colspan="2">01, 02, &hellip;, 12</td>
			</tr>
			<tr>
				<td><tt>%p</tt></td>
				<td>Locale’s equivalent of either AM or PM.</td>
				<td>
					AM, PM (en_US);<br />
					am, pm (de_DE)
				</td>
				<td>(1)</td>
			</tr>
			<tr>
				<td><tt>%M</tt></td>
				<td>Minute as a zero-padded decimal number.</td>
				<td colspan="2">00, 01, &hellip;, 59</td>
			</tr>
			<tr>
				<td><tt>%S</tt></td>
				<td>Second as a zero-padded decimal number.</td>
				<td>00, 01, &hellip;, 59</td>
				<td>(2)</td>
			</tr>
			<tr>
				<td><tt>%f</tt></td>
				<td>
					Microsecond as a decimal number, zero-padded on the left.
				</td>
				<td>000000, 000001, &hellip;, 999999</td>
				<td>(3)</td>
			</tr>
			<tr>
				<td><tt>%z</tt></td>
				<td>
					UTC offset in the form +HHMM or -HHMM (empty string if the
					object is naive).
				</td>
				<td>(empty), +0000, -0400, +1030</td>
				<td>(4)</td>
			</tr>
			<tr>
				<td><tt>%Z</tt></td>
				<td>Time zone name (empty string if the object is naive).</td>
				<td>(empty), UTC, EST, CST</td>
				<td>(4)</td>
			</tr>
			<tr>
				<td><tt>%j</tt></td>
				<td>Day of the year as a zero-padded decimal number.</td>
				<td colspan="2">001, 002, &hellip;, 366</td>
			</tr>
			<tr>
				<td><tt>%U</tt></td>
				<td>
					Week number of the year (Sunday as the first day of the
					week) as a zero padded decimal number. All days in a new
					year preceding the first Sunday are considered to be in
					week 0.
				</td>
				<td colspan="2">00, 01, &hellip;, 53</td>
			</tr>
			<tr>
				<td><tt>%W</tt></td>
				<td>
					Week number of the year (Monday as the first day of the
					week) as a decimal number. All days in a new year preceding
					the first Monday are considered to be in week 0.
				</td>
				<td colspan="2">00, 01, &hellip;, 53</td>
			</tr>
			<tr>
				<td><tt>%c</tt></td>
				<td>Locale’s appropriate date and time representation.</td>
				<td>
					Tue Aug 16 21:30:00 1988 (en_US);<br />
					Di 16 Aug 21:30:00 1988 (de_DE)
				</td>
				<td>(1)</td>
			</tr>
			<tr>
				<td><tt>%x</tt></td>
				<td>Locale’s appropriate date representation.</td>
				<td>
					08/16/88 (None);<br />
					08/16/1988 (en_US);<br />
					16.08.1988 (de_DE)
				</td>
				<td>(1)</td>
			</tr>
			<tr>
				<td><tt>%X</tt></td>
				<td>Locale’s appropriate time representation.</td>
				<td>
					21:30:00 (en_US);<br />
					21:30:00 (de_DE)
				</td>
				<td>(1)</td>
			</tr>
			<tr>
				<td><tt>%%</tt></td>
				<td>A literal <tt>%</tt> character.</td>
				<td>%</td>
			</tr>
        </table>
        <h4>table notes</h4>
        <ol>
			<li>
				Because the format depends on the current locale, care should be
				taken when making assumptions about the output value. Field
				orderings will vary (for example, &ldquo;month/day/year&rdquo;
				versus &ldquo;day/month/year&rdquo;), and the output may contain
				Unicode characters encoded using the locale’s default encoding
				(for example, if the current locale is <tt>ja_JP</tt>, the
				default encoding could be any one of <tt>eucJP</tt>,
				<tt>SJIS</tt>, or <tt>utf-8</tt>).
			</li>
			<li>
				Does not support leap seconds.
			</li>
			<li>
				<tt>%f</tt> is an extension to the set of format characters in
				the C standard (but implemented separately in datetime objects,
				and therefore always available).
			</li>
			<li>In umi, this will almost always be an empty string.</li>
        </ol>
        <hr />
        <ol>
            <li id="fn1">I say <q>will</q> because not everything is complete
            yet. I would like to add a command to umi (e.g.
            <code>umi status</code>) which quickly creates a status.
            [<a href="#rt1">return</a>]</li>
        </ol>
      </main>
      <footer id="site-footer"></footer>
    </body>
</html>
